package com.example.submission4.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.bumptech.glide.Glide;
import com.example.submission4.R;
import com.example.submission4.db.TvShowHelper;
import com.example.submission4.entity.TvShow;

public class DetailTvShow extends AppCompatActivity {
    public static final String EXTRA_FILM="extra_film";
    String name,release,popularity,overview;
    TextView tvName,tvRelease,tvPopularity,tvOverview;
    private boolean isFavorite = false;
    private TvShow tvShow;
    private ProgressBar progressBar;
    private MenuItem addFav, delFav;
    private TvShowHelper tvShowHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_tv_show);
        progressBar = findViewById(R.id.progressBar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.detail));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView img = findViewById(R.id.img_tvShow);
        tvName=findViewById(R.id.name_tvShow);
        tvRelease=findViewById(R.id.release_date);
        tvPopularity=findViewById(R.id.popularity_tvShow);
        tvOverview=findViewById(R.id.overview_tvShow);


        tvShow = getIntent().getParcelableExtra(EXTRA_FILM);

        Glide.with(this)
                .load(tvShow.getPoster())
                .into(img);
        name=tvShow.getName();
        tvName.setText(name);
        release=tvShow.getRelease();
        tvRelease.setText(release);
        popularity=tvShow.getPopularity();
        tvPopularity.setText(popularity);
        overview=tvShow.getOverview();
        tvOverview.setText(overview);

        tvShowHelper = TvShowHelper.getInstance(getApplicationContext());
        tvShowHelper.open();
        if (tvShowHelper.checkTvShowFav(tvShow.getId()))
            isFavorite = true;
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fav_menu, menu);
        addFav = menu.findItem(R.id.action_add_fav);
        delFav = menu.findItem(R.id.action_del_fav);

        if (isFavorite) {
            delFav.setVisible(true);
            addFav.setVisible(false);
        } else {
            delFav.setVisible(false);
            addFav.setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_add_fav:
                long insert = tvShowHelper.insertTvShowFav(tvShow);
                if (insert > 0) {
                    delFav.setVisible(true);
                    addFav.setVisible(false);
                    Toast.makeText(this, R.string.add_favorites, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.failed_add, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_del_fav:
                long delete = tvShowHelper.deleteTvShowFav(tvShow.getId());
                if (delete > 0) {
                    delFav.setVisible(false);
                    addFav.setVisible(true);
                    Toast.makeText(this, R.string.deleted_successfully, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.failed_delete, Toast.LENGTH_SHORT).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }


    }

}








