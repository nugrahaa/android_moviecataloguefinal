package com.example.submission4.activity;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.submission4.R;
import com.example.submission4.db.MovieHelper;
import com.example.submission4.entity.Movie;
import java.util.Objects;


public class DetailMovie extends AppCompatActivity  {
    public static final String EXTRA_FILM = "extra_film";
    private boolean isFavorite = false;
    private Movie movie;
    private MenuItem addFav, delFav;

    private MovieHelper movieHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_movie);
        ProgressBar progressBar = findViewById(R.id.progressBar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.detail));
        }

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        ImageView img = findViewById(R.id.img_movie);
        TextView tvTitle = findViewById(R.id.title_movie);
        TextView tvRelease = findViewById(R.id.release_date);
        TextView tvPopularity = findViewById(R.id.popularity_movie);
        TextView tvOverview = findViewById(R.id.overview_movie);

        movie = getIntent().getParcelableExtra(EXTRA_FILM);

        Glide.with(this)
                .load(movie.getPoster())
                .apply(new RequestOptions().override(350, 550))
                .into(img);
        tvTitle.setText(movie.getTitle());
        tvRelease.setText(movie.getRelease());
        tvPopularity.setText(movie.getPopularity());
        tvOverview.setText(movie.getOverview());

        movieHelper = MovieHelper.getInstance(getApplicationContext());
        movieHelper.open();
        if (movieHelper.checkMovieFav(movie.getId()))
            isFavorite = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        movieHelper.close();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fav_menu, menu);
        addFav = menu.findItem(R.id.action_add_fav);
        delFav = menu.findItem(R.id.action_del_fav);

        if (isFavorite) {
            delFav.setVisible(true);
            addFav.setVisible(false);
        } else {
            delFav.setVisible(false);
            addFav.setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_add_fav:
                long insert = movieHelper.insertMovieFav(movie);
                if (insert > 0) {
                    delFav.setVisible(true);
                    addFav.setVisible(false);
                    Toast.makeText(this,R.string.add_favorites, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.failed_add, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_del_fav:
                long delete = movieHelper.deleteMovieFav(movie.getId());
                if (delete > 0) {
                    delFav.setVisible(false);
                    addFav.setVisible(true);
                    Toast.makeText(this, R.string.deleted_successfully, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.failed_delete, Toast.LENGTH_SHORT).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }


    }

        }



























