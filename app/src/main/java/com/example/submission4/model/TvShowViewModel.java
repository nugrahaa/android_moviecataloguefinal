package com.example.submission4.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.submission4.MainActivity;
import com.example.submission4.db.TvShowHelper;
import com.example.submission4.entity.TvShow;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class TvShowViewModel extends ViewModel implements LoadTvShowFavCallback {
    private MutableLiveData<ArrayList<TvShow>> listTvShow = new MutableLiveData<>();
    private final static String TAG = MainActivity.class.getSimpleName();
    private static final String API_KEY = "aa5800983b85fa080dd050858a8eb415";

    public void setTvShow(){
        AsyncHttpClient client= new AsyncHttpClient();
        final ArrayList<TvShow> listItems=new ArrayList<>();
        String url = "https://api.themoviedb.org/3/tv/top_rated?api_key=aa5800983b85fa080dd050858a8eb415&language=en-US";


        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String((responseBody));
                    JSONObject responseObject= new JSONObject(result);
                    JSONArray list= responseObject.getJSONArray("results");
                    for (int i=0; i<list.length();i++){
                        JSONObject object = list.getJSONObject(i);
                        TvShow tvShow1= new TvShow(object);
                        listItems.add(tvShow1);
                    }
                    listTvShow.postValue(listItems);
                } catch (JSONException e) {
                    Log.d(TAG,"Data can not save");
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", Objects.requireNonNull(error.getMessage()));
            }
        });
    }
    public LiveData<ArrayList<TvShow>> getTvShows(){
        return listTvShow;
    }

    public void setTvShowFav(Context context) {
        TvShowHelper tvShowHelper = TvShowHelper.getInstance(context);
        tvShowHelper.open();
        new LoadTvShowFavAsync(tvShowHelper,this).execute();
    }


    @Override
    public void preExecute() {
        Log.d("TvShowViewModel", "preExecute");
    }

    @Override
    public void postExecute(ArrayList<TvShow> tvShows) {
            listTvShow.postValue(tvShows);
            Log.d("TvShowViewModel", "postExecute" + tvShows.toString());


    }
}

class LoadTvShowFavAsync extends AsyncTask<Void, Void, ArrayList<TvShow>> {
    private static final String TAG= LoadTvShowFavAsync.class.getSimpleName();
    private final WeakReference<TvShowHelper> weakTvShowHelper;
    private final WeakReference<LoadTvShowFavCallback> weakCallback;

    LoadTvShowFavAsync(TvShowHelper tvShowHelper, LoadTvShowFavCallback callback) {
        weakTvShowHelper = new WeakReference<>(tvShowHelper);
        weakCallback = new WeakReference<>(callback);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        weakCallback.get().preExecute();
    }

    @Override
    protected ArrayList<TvShow> doInBackground(Void... voids) {
        return weakTvShowHelper.get().getAllTvShowFav();
    }

    @Override
    protected void onPostExecute(ArrayList<TvShow> tvShows) {
        super.onPostExecute(tvShows);
        weakCallback.get().postExecute(tvShows);
    }
}


interface LoadTvShowFavCallback {
        void preExecute();

        void postExecute(ArrayList<TvShow> listTvShowModel);


    }



