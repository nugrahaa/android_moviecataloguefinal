package com.example.submission4.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.submission4.MainActivity;
import com.example.submission4.db.MovieHelper;
import com.example.submission4.entity.Movie;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class MovieViewModel extends ViewModel implements LoadMovieFavCallback {
    private MutableLiveData<ArrayList<Movie>> listMovies = new MutableLiveData<>();
    public final static String TAG = MainActivity.class.getSimpleName();
    private static final String API_KEY = "aa5800983b85fa080dd050858a8eb415";

    public void setMovie() {
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<Movie> listItems = new ArrayList<>();

        String url = "https://api.themoviedb.org/3/discover/movie?api_key=aa5800983b85fa080dd050858a8eb415&language=en-US";
        client.get(url, new AsyncHttpResponseHandler() {
            @Override

            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                try {
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray ja = responseObject.getJSONArray("results");

                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject object = ja.getJSONObject(i);
                        Movie movie1 = new Movie(object);
                        listItems.add(movie1);
                    }
                    listMovies.postValue(listItems);

                } catch (JSONException e) {
                    Log.d(TAG, "Data tidak tersimpan!!");
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", Objects.requireNonNull(error.getMessage()));
            }

        });
    }


    public LiveData<ArrayList<Movie>> getMovies() {
        return listMovies;
    }

    public void setMovieFav(Context context) {
        MovieHelper movieHelper = MovieHelper.getInstance(context);
        movieHelper.open();
        new LoadMovieFavAsync(movieHelper, this).execute();
    }


    @Override
    public void preExecute() {
        Log.d("MovieViewModel", "preExecute");
    }

    @Override
    public void postExecute(ArrayList<Movie> movies) {
        listMovies.postValue(movies);
        Log.d("MovieViewModel", "postExecute" + movies.toString());
    }
}

    class LoadMovieFavAsync extends AsyncTask<Void, Void, ArrayList<Movie>> {
        private static final String TAG = LoadMovieFavAsync.class.getSimpleName();
        private final WeakReference<MovieHelper> weakMovieHelper;
        private final WeakReference<LoadMovieFavCallback> weakCallback;

        LoadMovieFavAsync(MovieHelper movieHelper, LoadMovieFavCallback callback) {
            weakMovieHelper = new WeakReference<>(movieHelper);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected ArrayList<Movie> doInBackground(Void... voids) {
            return weakMovieHelper.get().getAllMoviesFav();
        }

        @Override
        protected void onPostExecute(ArrayList<Movie> movies) {
            super.onPostExecute(movies);
            weakCallback.get().postExecute(movies);
        }
    }

    interface LoadMovieFavCallback {
        void preExecute();

        void postExecute(ArrayList<Movie> listMovieModel);
    }
















