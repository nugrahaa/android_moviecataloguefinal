package com.example.submission4.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.submission4.activity.DetailTvShow;
import com.example.submission4.R;
import com.example.submission4.adapter.TvShowAdapter;
import com.example.submission4.entity.TvShow;
import com.example.submission4.model.TvShowViewModel;
import java.util.ArrayList;

import static com.example.submission4.fragment.MovieFragment.EXTRA_STATE_FOR_FAV;

public class TvShowFragment extends Fragment {
    private ProgressBar progressBar;
    private TvShowAdapter adapter;
    private TvShowViewModel tvShowViewModel ;
    private boolean isForFavActivity, shouldRefreshOnResume = false;


    public TvShowFragment(){
    }
    public TvShowFragment(boolean isForFavActivity) {
        this.isForFavActivity = isForFavActivity;
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv_show, container, false);

            progressBar = view.findViewById(R.id.progressBar);
            showLoading(true);


        if (savedInstanceState != null) {
            isForFavActivity = savedInstanceState.getBoolean(EXTRA_STATE_FOR_FAV);
        }

        tvShowViewModel = ViewModelProviders.of(getActivity()).get(TvShowViewModel.class);

        if (isForFavActivity) {
            tvShowViewModel.setTvShowFav(getActivity());
        } else {
            tvShowViewModel.setTvShow();
        }

        tvShowViewModel.getTvShows().observe(getActivity(), getTvShow);

        adapter= new TvShowAdapter();
        adapter.notifyDataSetChanged();

        RecyclerView rvView = view.findViewById(R.id.movie_rv);
        rvView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvView.setAdapter(adapter);

        adapter.setOnItemClickCallback(new TvShowAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(TvShow tvShow) {
                Toast.makeText(getActivity(), getResources().getString(R.string.toast_text) + " " + tvShow.getName(), Toast.LENGTH_SHORT).show();
                Intent moveWithObjectIntent = new Intent(getContext(), DetailTvShow.class);
                moveWithObjectIntent.putExtra(DetailTvShow.EXTRA_FILM, tvShow);
                startActivity(moveWithObjectIntent);
            }
        });
        return view;
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_STATE_FOR_FAV, isForFavActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldRefreshOnResume) {
            if (isForFavActivity) {
                tvShowViewModel.setTvShowFav(getActivity());
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        shouldRefreshOnResume = true;
    }


    private Observer<ArrayList<TvShow>> getTvShow = new Observer<ArrayList<TvShow>>() {
        @Override
        public void onChanged(@Nullable ArrayList<TvShow> tvShowList) {
            if (tvShowList != null) {
                adapter.setTvShowList(tvShowList);
                Log.d("Data list TvShow", adapter.getTvShowList().toString());
                showLoading(false);
            }
        }
    };

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }

    }

    }


