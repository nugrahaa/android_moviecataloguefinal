package com.example.submission4.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.submission4.activity.DetailMovie;
import com.example.submission4.R;
import com.example.submission4.adapter.MovieAdapter;
import com.example.submission4.entity.Movie;
import com.example.submission4.model.MovieViewModel;
import java.util.ArrayList;
import java.util.Objects;

public class MovieFragment extends Fragment  {
    public static final String EXTRA_STATE_FOR_FAV = "extra_state_for_fav";
    private ProgressBar progressBar;
    private MovieAdapter adapter;
    private MovieViewModel movieViewModel;
    private boolean isForFavActivity, shouldRefreshOnResume = false;


    public MovieFragment(){
    }
    public MovieFragment(boolean isForFavActivity) {
        this.isForFavActivity = isForFavActivity;
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
       View v;
        v = inflater.inflate(R.layout.fragment_movie, container, false);
        return v;
    }
        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
            super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progressBar);
        showLoading(true);


        if (savedInstanceState != null) {
            isForFavActivity = savedInstanceState.getBoolean(EXTRA_STATE_FOR_FAV);
        }

        movieViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MovieViewModel.class);

        if (isForFavActivity) {
            movieViewModel.setMovieFav(getActivity());
        } else {
            movieViewModel.setMovie();
        }

        movieViewModel.getMovies().observe(getActivity(), getMovie);

        adapter = new MovieAdapter();
        adapter.notifyDataSetChanged();

        RecyclerView rvView = view.findViewById(R.id.movie_rv);
        rvView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvView.setAdapter(adapter);

        adapter.setOnItemClickCallback(new MovieAdapter.OnItemClickCallback() {
                @Override
                public void onItemClicked(Movie movie) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.toast_text) + " " + movie.getTitle(), Toast.LENGTH_SHORT).show();
                    Intent moveWithObjectIntent = new Intent(getContext(), DetailMovie.class);
                    moveWithObjectIntent.putExtra(DetailMovie.EXTRA_FILM, movie);
                    startActivity(moveWithObjectIntent);
                }
            });
        }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_STATE_FOR_FAV, isForFavActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldRefreshOnResume) {
            if (isForFavActivity) {
                movieViewModel.setMovieFav(getActivity());
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        shouldRefreshOnResume = true;
    }


    private Observer<ArrayList<Movie>> getMovie = new Observer<ArrayList<Movie>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Movie> movieList) {
            if (movieList != null) {
                adapter.setMovieList(movieList);
                Log.d("Data list movie", adapter.getMovieList().toString());
                showLoading(false);
            }
        }
    };

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }

    }
}
















