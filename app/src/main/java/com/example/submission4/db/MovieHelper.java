package com.example.submission4.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcel;
import android.util.Log;
import com.example.submission4.entity.Movie;
import java.util.ArrayList;
import static android.provider.BaseColumns._ID;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.ID_MOVIE;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.OVERVIEW;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.POPULARITY;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.POSTER;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.RELEASE;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.TITLE;
import static com.example.submission4.db.DatabaseContract.TABLE_FAVORITE_MOVIE;

public class MovieHelper {
    private static final String TAG = MovieHelper.class.getSimpleName();
    private static final String DATABASE_TABLE = TABLE_FAVORITE_MOVIE;
    private static DatabaseHelper databaseHelper;
    private static MovieHelper INSTANCE;

    private static SQLiteDatabase database;

    private MovieHelper(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public static MovieHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null)
                    INSTANCE = new MovieHelper(context);
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();
        if (database.isOpen())
            database.close();
    }

    public ArrayList<Movie> getAllMoviesFav() {
        ArrayList<Movie> arrayList = new ArrayList<>();
        Cursor cursor = database.query(DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " asc",
                null);
        cursor.moveToFirst();
        Movie movie;
        if (cursor.getCount() > 0) {
            do {
                movie = new Movie(Parcel.obtain());
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(ID_MOVIE)));
                movie.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movie.setRelease(cursor.getString(cursor.getColumnIndexOrThrow(RELEASE)));
                movie.setPopularity(cursor.getString(cursor.getColumnIndexOrThrow(POPULARITY)));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));

                arrayList.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public boolean checkMovieFav(int id) {
        String selectString = "SELECT * FROM " + TABLE_FAVORITE_MOVIE + " WHERE " + ID_MOVIE+ " =?";
        Cursor cursor = database.rawQuery(selectString, new String[]{String.valueOf(id)});
        boolean checkMovieFav = false;
        if (cursor.moveToFirst()) {
            checkMovieFav = true;
            int count = 0;
            while (cursor.moveToNext())
                count++;
            Log.d(TAG, String.format("%d record found", count));
        }
        cursor.close();
        return checkMovieFav;
    }

    public long insertMovieFav(Movie movie) {
        ContentValues args = new ContentValues();
        args.put(POSTER, movie.getPoster());
        args.put(ID_MOVIE, movie.getId());
        args.put(POPULARITY, movie.getPopularity());
        args.put(TITLE, movie.getTitle());
        args.put(RELEASE, movie.getRelease());
        args.put(OVERVIEW, movie.getOverview());
        return database.insert(DATABASE_TABLE, null, args);
    }

    public int deleteMovieFav(int id) {
        return database.delete(DATABASE_TABLE, ID_MOVIE+ " = '" + id + "'", null);
    }

}
