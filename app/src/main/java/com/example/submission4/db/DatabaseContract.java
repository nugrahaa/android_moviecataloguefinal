package com.example.submission4.db;

import android.provider.BaseColumns;
import androidx.room.Entity;

@Entity
class DatabaseContract {
        static String TABLE_FAVORITE_MOVIE = "movie_favorite";
        static String TABLE_FAVORITE_TV_SHOW = "tv_show_favorite";

        static final class FavoriteColumn implements BaseColumns {
            static String ID_MOVIE= "id";
            static String OVERVIEW = "overview";
            static String RELEASE = "release_date";
            static String POSTER= "poster_path";
            static String POPULARITY = "popularity";
            static String TITLE = "title";

        }

        static final class TvShowFavColumns implements BaseColumns{
            static String ID_TV_SHOW= "id";
            static String OVERVIEW = "overview";
            static String POSTER = "poster_path";
            static String POPULARITY = "popularity";
            static String RELEASE = "release_date";
            static String NAME = "name";
        }
    }

