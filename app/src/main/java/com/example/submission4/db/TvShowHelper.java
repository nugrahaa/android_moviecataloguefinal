package com.example.submission4.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcel;
import android.util.Log;
import com.example.submission4.entity.TvShow;
import java.util.ArrayList;
import static android.provider.BaseColumns._ID;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.OVERVIEW;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.POPULARITY;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.POSTER;
import static com.example.submission4.db.DatabaseContract.FavoriteColumn.RELEASE;
import static com.example.submission4.db.DatabaseContract.TABLE_FAVORITE_TV_SHOW;
import static com.example.submission4.db.DatabaseContract.TvShowFavColumns.ID_TV_SHOW;
import static com.example.submission4.db.DatabaseContract.TvShowFavColumns.NAME;

public class TvShowHelper {
    private static final String TAG = TvShowHelper.class.getSimpleName();
    private static final String DATABASE_TABLE = TABLE_FAVORITE_TV_SHOW;
    private static DatabaseHelper databaseHelper;
    private static TvShowHelper INSTANCE;

    private static SQLiteDatabase database;

    private TvShowHelper(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public static TvShowHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null)
                    INSTANCE = new TvShowHelper(context);
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();
        if (database.isOpen())
            database.close();
    }

    public ArrayList<TvShow> getAllTvShowFav() {
        ArrayList<TvShow> arrayList = new ArrayList<>();
        Cursor cursor = database.query(DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " asc",
                null);
        cursor.moveToFirst();
        TvShow tvShow;
        if (cursor.getCount() > 0) {
            do {
                tvShow = new TvShow(Parcel.obtain());
                tvShow.setId(cursor.getInt(cursor.getColumnIndexOrThrow(ID_TV_SHOW)));
                tvShow.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));
                tvShow.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
                tvShow.setRelease(cursor.getString(cursor.getColumnIndexOrThrow(RELEASE)));
                tvShow.setPopularity(cursor.getString(cursor.getColumnIndexOrThrow(POPULARITY)));
                tvShow.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));

                arrayList.add(tvShow);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public boolean checkTvShowFav(int id) {
        String selectString = "SELECT * FROM " + TABLE_FAVORITE_TV_SHOW + " WHERE " + ID_TV_SHOW+ " =?";
        Cursor cursor = database.rawQuery(selectString, new String[]{String.valueOf(id)});
        boolean checkTvShowFav = false;
        if (cursor.moveToFirst()) {
            checkTvShowFav = true;
            int count = 0;
            while (cursor.moveToNext())
                count++;
            Log.d(TAG, String.format("%d record found", count));
        }
        cursor.close();
        return checkTvShowFav;
    }

    public long insertTvShowFav(TvShow tvShow) {
        ContentValues args = new ContentValues();
        args.put(ID_TV_SHOW, tvShow.getId());
        args.put(POSTER, tvShow.getPoster());
        args.put(RELEASE, tvShow.getRelease());
        args.put(OVERVIEW, tvShow.getOverview());
        args.put(POPULARITY, tvShow.getPopularity());
        args.put(NAME, tvShow.getName());
        return database.insert(DATABASE_TABLE, null, args);
    }

    public int deleteTvShowFav(int id) {
        return database.delete(DATABASE_TABLE, ID_TV_SHOW + "= '" + id + "'", null);
    }

}
