package com.example.submission4.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME = "dbfavapp";
    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_MOVIE = String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +//ID
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL)",
            DatabaseContract.TABLE_FAVORITE_MOVIE,
            DatabaseContract.FavoriteColumn._ID,
            DatabaseContract.FavoriteColumn.ID_MOVIE,
            DatabaseContract.FavoriteColumn.POSTER,
            DatabaseContract.FavoriteColumn.TITLE,
            DatabaseContract.FavoriteColumn.RELEASE,
            DatabaseContract.FavoriteColumn.POPULARITY,
            DatabaseContract.FavoriteColumn.OVERVIEW

    );
    private static final String SQL_CREATE_TABLE_TV_SHOW = String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL," +
                    " %s TEXT NOT NULL)",
            DatabaseContract.TABLE_FAVORITE_TV_SHOW,
            DatabaseContract.TvShowFavColumns._ID,
            DatabaseContract.TvShowFavColumns.ID_TV_SHOW,
            DatabaseContract.TvShowFavColumns.POSTER,
            DatabaseContract.TvShowFavColumns.NAME,
            DatabaseContract.TvShowFavColumns.RELEASE,
            DatabaseContract.TvShowFavColumns.POPULARITY,
            DatabaseContract.TvShowFavColumns.OVERVIEW
    );


    DatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_MOVIE);
        db.execSQL(SQL_CREATE_TABLE_TV_SHOW);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_FAVORITE_MOVIE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_FAVORITE_TV_SHOW);
        onCreate(db);
    }
    }

