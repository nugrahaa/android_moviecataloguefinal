package com.example.submission4.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.submission4.R;
import com.example.submission4.entity.Movie;
import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {
    private ArrayList<Movie> movieList = new ArrayList<>();
    private OnItemClickCallback onItemClickCallback;


    public ArrayList<Movie>  getMovieList() {
        return movieList;
    }

    public void setMovieList(ArrayList<Movie> data) {
        movieList.clear();
        movieList.addAll(data);
        notifyDataSetChanged();
    }
    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }
    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie1, viewGroup, false);
        return new MyViewHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MyViewHolder myViewHolder,  int position) {
        myViewHolder.bind(movieList.get(position));

    }

    @Override
    public int getItemCount() {

        return movieList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvPopularity;
        TextView tvTitle;
        TextView tvRelease;
        TextView tvOverview;

       MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_movie);
            tvPopularity=itemView.findViewById(R.id.popularity_movie);
            tvOverview=itemView.findViewById(R.id.overview_movie);
            tvRelease=itemView.findViewById(R.id.release_date);
            tvTitle = itemView.findViewById(R.id.title_movie);

           }

        void bind(final Movie movie) {
            Glide.with(itemView.getContext())
                    .load(movie.getPoster())
                    .apply(new RequestOptions().override(350, 550))
                    .into(img);
            tvTitle.setText(movie.getTitle());
            tvOverview.setText(movie.getOverview());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickCallback.onItemClicked(movieList.get(getAdapterPosition()));
                }
            });
        }
    }
    public interface OnItemClickCallback {
        void onItemClicked(Movie movie);
    }
}













