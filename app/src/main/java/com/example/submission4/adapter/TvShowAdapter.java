package com.example.submission4.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.submission4.R;
import com.example.submission4.entity.TvShow;

import java.util.ArrayList;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.MyViewHolder> {
    private ArrayList<TvShow> tvShowList = new ArrayList<>();
    private OnItemClickCallback onItemClickCallback;


    public ArrayList<TvShow>  getTvShowList() {
        return tvShowList;
    }

    public void setTvShowList(ArrayList<TvShow> data) {
        tvShowList.clear();
        tvShowList.addAll(data);
        notifyDataSetChanged();
    }
    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }
    @NonNull
    @Override
    public TvShowAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie1, viewGroup, false);
        return new TvShowAdapter.MyViewHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull TvShowAdapter.MyViewHolder myViewHolder,  int position) {
        myViewHolder.bind(tvShowList.get(position));

    }

    @Override
    public int getItemCount() {

        return tvShowList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvPopularity;
        TextView tvOverview;
        TextView tvRelease;
        TextView tvTitle;
        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_movie);
            tvRelease=itemView.findViewById(R.id.release_date);
            tvOverview=itemView.findViewById(R.id.overview_movie);
            tvPopularity=itemView.findViewById(R.id.popularity_movie);
            tvTitle= itemView.findViewById(R.id.title_movie);

        }

        void bind(final TvShow tvShow) {
            Glide.with(itemView.getContext())
                    .load(tvShow.getPoster())
                    .apply(new RequestOptions().override(300, 500))
                    .into(img);
            tvTitle.setText(tvShow.getName());
            tvOverview.setText(tvShow.getOverview());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickCallback.onItemClicked(tvShowList.get(getAdapterPosition()));
                }
            });
        }
    }
    public interface OnItemClickCallback {
        void onItemClicked(TvShow tvShow);
    }
}



