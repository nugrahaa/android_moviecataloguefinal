package com.example.submission4.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.example.submission4.model.MovieViewModel;
import org.json.JSONException;
import org.json.JSONObject;

public class TvShow implements Parcelable {

    private int id;
    private String popularity;
    private String poster;
    private String name;
    private String overview;
    private String release;



    private final String url="https://image.tmdb.org/t/p/original/";

    public  TvShow(JSONObject object) {
        try {
            int id=object.getInt("id");
            String popularity = object.getString("popularity");
            String poster = object.getString("poster_path");
            String overview = object.getString("overview");
            String release = object.getString("first_air_date");
            String name = object.getString("name");

            this.id=id;
            this.poster = url + poster;
            this.name = name;
            this.release = release;
            this.popularity = popularity;
            this.overview = overview;
            Log.i(MovieViewModel.TAG, this.name);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(MovieViewModel.TAG, "Something Error");
        }
    }



    public int getId() {
        return id;
    }
    public void setId(int id){
        this.id=id;
    }

    public String getPoster() {
        return poster;
    }

    public  void setPoster(String poster){this.poster=poster;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release){this.release=release;}

    public String getPopularity() {
        return popularity;
    }
    public void  setPopularity(String popularity){this.popularity=popularity;}

    public String getOverview() {
        return overview;
    }
    public void setOverview(String overview){this.overview=overview;}




    @Override
    public int describeContents() {
        return 0;
    }



    public TvShow(Parcel in) {
        id=in.readInt();
        poster = in.readString();
        name = in.readString();
        release = in.readString();
        popularity = in.readString();
        overview = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(poster);
        dest.writeString(name);
        dest.writeString(release);
        dest.writeString(popularity);
        dest.writeString(overview);
        dest.writeString(url);
    }


    public static final Creator<TvShow> CREATOR = new Creator<TvShow>() {
        @Override
        public TvShow createFromParcel(Parcel in) {
            return new TvShow(in);
        }

        @Override
        public TvShow[] newArray(int size) {
            return new TvShow[size];
        }
    };
    }
