package com.example.submission4.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.example.submission4.model.MovieViewModel;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Movie implements Parcelable {

    private int id;
    private String poster;
    private String popularity;
    private String title;
    private String release;
    private String overview;
    private final String url="https://image.tmdb.org/t/p/original/";


    public  Movie(JSONObject object){
        try {
            int id= object.getInt("id");
            String poster = object.getString("poster_path");
            String title  = object.getString("title");
            String release =object.getString("release_date");
            String popularity =object.getString("popularity");
            String overview = object.getString("overview");
            this.id=id;
            this.poster=url+ poster;
            this.title=title;
            this.release=release;
            this.popularity=popularity;
            this.overview=overview;
            Log.i(MovieViewModel.TAG,this.title);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(MovieViewModel.TAG,"Something error");
        }
    }

    public Movie(Parcel in) {
        id=in.readInt();
        poster = in.readString();
        title = in.readString();
        release=in.readString();
        popularity = in.readString();
        overview = in.readString();
    }


    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };



    public int getId(){return id;}
    public void setId(int id) {
        this.id = id;
    }

    public String getPoster() {
        return poster;
    }
    public void setPoster(String poster){this.poster=poster;}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease(){
        return release;
    }
    public void setRelease(String release){this.release=release;}


    public String getPopularity() {
        return popularity;
    }
    public void setPopularity(String popularity){this.popularity=popularity;}


    public String getOverview() {
        return overview;
    }
    public void setOverview(String overview){this.overview=overview;}

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(poster);
        dest.writeString(title);
        dest.writeString(release);
        dest.writeString(popularity);
        dest.writeString(overview);
        dest.writeString(url);
    }
}



